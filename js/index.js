$(function () {
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
	$('.carousel').carousel({
		interval: 1000
	});

	$('#contacto').on('show.bs.modal', function(event) {
		console.log("El modal contacto se esta mostrando");
		$('.contactoBtn').removeClass('btn-outline-warning');
		$('.contactoBtn').addClass('btn-primary');
		$('.contactoBtn').prop('disabled',true);
	});
	$('#contacto').on('shown.bs.modal', function(event) {
		console.log("El modal contacto se mostró");
	});
	$('#contacto').on('hide.bs.modal', function(event) {
		console.log("El modal contacto se oculta");
	});
	$('#contacto').on('hidden.bs.modal', function(event) {
		console.log("El modal contacto se ocultó");
		$('.contactoBtn').removeClass('btn-primary');
		$('.contactoBtn').addClass('btn-outline-warning');
		$('.contactoBtn').prop('disabled',false);
	});
});